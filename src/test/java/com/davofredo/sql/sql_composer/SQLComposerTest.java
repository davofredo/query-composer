package com.davofredo.sql.sql_composer;

import com.davofredo.util.ListUtil;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

class SQLComposerTest {

    @Test
    void testConcatWhereClause() {
        String nameIsNotNull = "name IS NOT NULL";
        String sqlSelect = "SELECT * FROM table_1 WHERE ${nameIsNotNull}";
        String completeQuery = "SELECT * FROM table_1 WHERE name IS NOT NULL";

        SQLComposer sqlComposer = new SQLComposer();
        sqlComposer.compose("nameIsNotNull", nameIsNotNull);
        Assertions.assertEquals(completeQuery, sqlComposer.compose(sqlSelect));
    }

    @Test
    void testConcatColumnsAndWhere() {
        String[] columns = new String[] {
                "column1",
                "column2",
                "column3"
        };
        String columnList = ListUtil.toStringList(columns);
        String nameIsNotNull = "column1 IS NOT NULL";
        String sqlSelect = "SELECT ${selectColumns} FROM table_1 WHERE ${nameIsNotNull}";
        String completeQuery = "SELECT column1, column2, column3 FROM table_1 WHERE column1 IS NOT NULL";

        SQLComposer sqlComposer = new SQLComposer();
        sqlComposer.compose("selectColumns", columnList);
        sqlComposer.compose("nameIsNotNull", nameIsNotNull);
        Assertions.assertEquals(completeQuery, sqlComposer.compose(sqlSelect));
    }

    @Test
    void testConcatColumnsAndWhereWithParams() {
        String[] columns = new String[] {
                "${selectColumns.table.alias}.column1",
                "${selectColumns.table.alias}.column2",
                "${selectColumns.table.alias}.column3"
        };
        String columnList = ListUtil.toStringList(columns);
        String nameIsNotNull = "${nameIsNotNull.table.alias}.column1 IS NOT NULL";
        String sqlSelect = "SELECT ${selectColumns} FROM table_1 t1 WHERE ${nameIsNotNull}";
        String completeQuery = "SELECT t1.column1, t1.column2, t1.column3 FROM table_1 t1 WHERE t1.column1 IS NOT NULL";

        Map<String, String> params = new HashMap<String, String>();
        params.put("selectColumns.table.alias", "t1");
        params.put("nameIsNotNull.table.alias", "t1");

        SQLComposer sqlComposer = new SQLComposer();
        sqlComposer.compose("selectColumns", columnList);
        sqlComposer.compose("nameIsNotNull", nameIsNotNull);
        Assertions.assertEquals(completeQuery, sqlComposer.compose(sqlSelect, params));
    }

}
