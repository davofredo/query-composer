package com.davofredo.sql.sql_composer;

import org.apache.commons.lang.text.StrSubstitutor;

import java.util.HashMap;
import java.util.Map;

public class SQLComposer {

    private Map<String, String> sqlComponents = new HashMap<String, String>();

    /**
     * Composes the provided sql element, by replacing all text holders with corresponding previously registered sql
     * elements.
     *
     * @param sql SQL code representing a full query or partial SQL code
     * @return The composed query
     */
    public String compose(String sql) {
        return compose(null, sql, null);
    }

    /**
     * Composes the provided sql element, by replacing all text holders with corresponding previously registered sql
     * elements.
     *
     * @param sql SQL code representing a full query or partial SQL code
     * @return The composed query
     */
    public String compose(String sql, Map<String, String> params) {
        return compose(null, sql, params);
    }

    /**
     * Composes the provided sql element, by replacing all text holders with corresponding previously registered sql
     * elements.
     *
     * @param sqlIdentifier Unique identifier for this sql element. If null, the element wont' be registered
     * @param sql SQL code representing a full query or partial SQL code
     * @return The composed query
     */
    public String compose(String sqlIdentifier, String sql) {
        return compose(sqlIdentifier, sql, null);
    }

    /**
     * Composes the provided sql elementComposes the provided sql element, by replacing all text holders with corresponding previously registered sql
     * elements., by replacing all text holders with corresponding previously registered sql
     * elements.
     *
     * @param sqlIdentifier Unique identifier for this sql element. If null, the element wont' be registered
     * @param sql SQL code representing a full query or partial SQL code
     * @param params Parameter values that may be required by to fill query gaps or to customize query elements
     * @return The composed query
     */
    public String compose(String sqlIdentifier, String sql, Map<String, String> params) {

        // Prevent from overriding sqlIdentifiers
        if(sqlIdentifier != null && sqlComponents.containsKey(sqlIdentifier))
            throw new RuntimeException("A SQL Component with key \"" + sqlIdentifier + "\" is already defined.");

        // Compose
        StrSubstitutor compSubstitutor = new StrSubstitutor(sqlComponents);
        String result = compSubstitutor.replace(sql);

        // If there are params, setup and fill with param values
        if(params != null && !params.isEmpty()) {
            /*
            result
                    .replace("#{{", "${")
                    .replace("}}", "}");
             */

            // Fill params
            StrSubstitutor paramSubstitutor = new StrSubstitutor(params);
            result = paramSubstitutor.replace(result);
        }

        // If a identifier has been provided, register the composite sql
        if(sqlIdentifier != null) sqlComponents.put(sqlIdentifier, result);

        return result;
    }

}
